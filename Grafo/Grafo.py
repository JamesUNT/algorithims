class Grafo:
    def __init__(self):
        # Lista de nodos totais
        self.lista_de_nodos = []
        # Pilha de visitação
        self.pilha_de_visitacao = []
        # Lista (de tuplas) das relaçôes entre os nodos e seu custo
        self.arestas = []
        # Pilha de borda (armazena os valores dos nodos da visitação)
        self.borda = []
        # Lista do caminho percorrido pelo algoritmo
        self.caminho = []
        # Custo total do grafo
        self.custo_total = 0
        # Custo total do caminho percorrido no grafo
        self.custo_por_iteracao = 0

    def adiciona_nodo(self, nodo):
        self.lista_de_nodos.append(nodo)

    def adiciona_adjacencia(self, nodo_alvo, nodo_filho, custo):
        self.arestas.append((nodo_alvo, nodo_filho, custo))
        nodo_alvo.adjacencia.append(nodo_filho)

        self.custo_total += custo

    def exibir_conclusoes(self):
        print("\n")
        print("--Conclusões--")
        print("Estrutura do grafo:")
        for item in self.lista_de_nodos:
            print("Nodo ->", item.data, end=" |")
            print(" Filhos: {", end=" ")
            for adjacencia in item.adjacencia:
                print("<" + adjacencia.data + ">", end=" ")
            print("}", end="\n")
        print(f"Custo total do grafo: {self.custo_total}")
        print(f"Custo do caminho percorrido: {self.custo_por_iteracao}")
        print("Caminho percorrido: " + " -> ".join(self.caminho))
