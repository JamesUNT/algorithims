def DFS(grafo, nodo_atual, nodo_objetivo):
    while True:
        # Se o nodo atual for nulo, retorne nulo
        if nodo_atual is None:
            return None

        # Registra o nodo atual que está sendo visitado.
        grafo.caminho.append(nodo_atual.data)

        # Verifica se o nodo atual é o nodo objetivo, se for, pare o laço while.
        if nodo_atual is nodo_objetivo:
            break

        # Atualize o estado do nodo para "visitado".
        nodo_atual.visitado = True

        # Após a descobeta do nodo, adicionar seus filhos na pilha de visitação.
        for filho in nodo_atual.adjacencia:
            # Somente adicione a pilha_de_visitacao se o nodo não tiver sido visitado
            if filho.visitado is False:
                grafo.pilha_de_visitacao.append(filho)
                grafo.borda.append(filho.data)

        # Printe o conteudo do nodo, a pilha de visitação e o camiho percorrido.
        print("nodo atual -> ", nodo_atual.data)
        print("Borda: ", grafo.borda)
        print("\n")

        # execute se a pilha de verificação não estiver vazia e o ultimo elemento (prox. nodo) não foi percorrido
        if (
            len(grafo.pilha_de_visitacao) != 0
            and grafo.pilha_de_visitacao[-1].visitado is False
        ):
            # nodo_atual recebe o último elemento da pilha de visitação
            valor_da_iteracao = [
                item[2]
                for item in grafo.arestas
                if item[0] is nodo_atual and item[1] is grafo.pilha_de_visitacao[-1]
            ]
            grafo.custo_por_iteracao += valor_da_iteracao[0]
            print(f"custo por iteracao: {grafo.custo_por_iteracao}")
            nodo_atual = grafo.pilha_de_visitacao.pop()
            grafo.borda.pop()
        else:
            print("borda vazia, grafo sem solução.")
            break
    # Se o while saiu, então quer dizer que ele achou o nodo objetivo ou chegou ao final da árvore,
    # então retorne uma mensagem
    return "Algoritimo finalizado !!"
